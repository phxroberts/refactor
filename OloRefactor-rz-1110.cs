﻿using System;
using System.Collections.Generic;

//Please see notes at bottom of the file for work flow explanation and potential improvements

namespace Refactoring
{

    public class Program
    {
        public static void Main(string[] args)
        {
            Order order = initializeOrder();
            order.PrintOrder();
            Console.ReadKey();
        }

        private static Order initializeOrder()
        {
            return new Order
            {
                Name = "John Doe",
                Products = new List<Product>
                {
                    new Product
                    {
                        ProductName = "Pulled Pork",
                        Price = 6.99m,
                        Units = new PerPoundUnits { Amount=0.5m }
                    },
                    new Product
                    {
                        ProductName = "Coke",
                        Price = 3m,
                        Units = new PerItemUnits { Amount=2 }
                    }
                }
            };
        }
    }

    public class Order
    {
        public string Name { get; set; }
        public List<Product> Products { get; set; }

        public void PrintOrder() {

            var price = 0m;
            var orderSummary = "ORDER SUMMARY FOR " + Name + ": \r\n";
            foreach (var orderProduct in Products)
            {
                price += orderProduct.PrintProduct(ref orderSummary);
                orderSummary += "\r\n";
            }

            Console.WriteLine(orderSummary);
            Console.WriteLine("Total Price: $" + price);
        }
    }

    public abstract class ProductUnits
    {
        public abstract void PrintUnitsCost(decimal price, ref string orderSummary);
        public abstract decimal CalculateUnitsCost(decimal price);
    }
    public class PerPoundUnits : ProductUnits
    {
        public decimal Amount { get; set; }
        public override void PrintUnitsCost(decimal price, ref string orderSummary)
        {
            orderSummary += (" $" + CalculateUnitsCost(price) + " (" + Amount + " pounds at $" + price + " per pound)");
        }
        public override decimal CalculateUnitsCost(decimal price)
        {
            return Amount*price;
        }
    }
    public class PerItemUnits : ProductUnits
    {
        public int Amount { get; set; }
        public override void PrintUnitsCost(decimal price, ref string orderSummary)
        {
            orderSummary += (" $" + CalculateUnitsCost(price) + " (" + Amount + " items at $" + price + " each)");
        }
        public override decimal CalculateUnitsCost(decimal price)
        {
            return Amount * price;
        }
    }

    public class Product
    {
        public string ProductName { get; set; }
        public decimal Price { get; set; }
        public ProductUnits Units { get; set; }

        public decimal PrintProduct(ref string orderSummary)
        {
            var productPrice = 0m;
            orderSummary += ProductName;

            productPrice = Units.CalculateUnitsCost(Price);
            Units.PrintUnitsCost(Price, ref orderSummary);

            return productPrice;
        }
    }
}


//My work flow and a rough mapping to the associated refactorings are below (refactorings in quotes on the right)

//1) Hide public fields for Product class - "Encapsulate Field"

//2) Replace Tuple with Order class - "Replace array with object"

//3) Move initialization of orders list to it's own funciton - "Extract Method"

//4) Move print loop it's own function - "Extract Method"

//4) Move print function into Order object - "Move Method/Remove Parameter"

//5) Extract product printing to it's own function  - "Extract Method"

//6) Moved total price accumulation out of conditional - "Consolodate Conditional Fragments"

//7) Moved price accumulation out of product print function - "Remove Parameter"

//8) Move product printing function into Product object - "Move Method/Remove Parameter"

//9) Weight and Quantity are clumsy and getting in each others way. Units for product need to be abstracted. - "Replace Conditional with Polymorphism"
//    Add base class "ProductUnits" with abstract functions PrintUnitsCost and CalculateUnitsCost
//    Add Derived classes "PerPoundUnits" and "PerItemUnits"
//    Replace Weight, Quantity, and PricingMethod with ProductUnits


/*
Potential Improvements:
There could be some generic intermediate classes between the base ProductUnits class and the derived PerPoundUnits and PerItemUnits that would 
be more resusable - DecimalUnits and IntegerUnits for example. The unit name ("pound" and "item") as well as the description ("per pound" and "each")
would be properties on the base ProductUnit and then the function PrintUnitsCost and CalculateUnitsCost could be moved up the hierachy as well. 
A class like PerOunceUnits would then derive from DecimalUnits.

class ProductUnits {
   public string Name { get; set; }
   public string Description { get; set; }


class DecimalUnits : ProductUnits {
   public virtual void PrintUnitsCost() {}  //Has all info needed to print for derived
   public virtual decimal CalculateUnitsCost(decimal price) {}  //Has all info needed to calculate for derived


class PerOunceUnits : DecimalUnits {
   public PerOunceUnits() {
      Name="ounce";
      Description="per fluid ounce";
   }
*/